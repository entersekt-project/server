const express = require('express');
const router = express.Router();
const loginHistory = require('../handlers/loginHistory')

/* GET /api/loginHistory */
router.get('/:id', async (req, res) => {
  try {
    let parts
    if (req.headers.authorization) {
      parts = req.headers.authorization.split(/\s+/)
    }
    const loginHist = await loginHistory.getLoginHistory(req.params.id, parts[1])
    res.json(loginHist);
  } catch (error) {
    res.status(500).json({ error });
  }
});

module.exports = router;