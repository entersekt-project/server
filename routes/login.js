const express = require('express');
const router = express.Router();
const userHandler = require('../handlers/user.js');
const loginHandler = require('../handlers/loginHistory');
/* GET /api/login */
router.get('/', async (req, res) => {
  try {
    const login = await userHandler.authenticate(req, res);        
    res.json(login);
  } catch (error) {
    res.status(500).json({ error });
  }
});

/* POST /api/login */
router.post('/', async (req, res) => {
  try {
    const userDetails = await userHandler.createUser(req.body.params);   
    res.json(userDetails);
  } catch (error) {
    console.log(error)
    res.status(500).json({ error });
  }
});

router.post('/auth', async (req, res) => {
  try {
    let loginUser = await userHandler.authenticateUser(req.body.params);   
    if(loginUser){
      const logEntry = await loginHandler.createLoginEntry(loginUser)
    }   
    res.json(loginUser);
  } catch (error) {
    console.log(error)
    res.status(500).json({ error });
  }
});


module.exports = router;