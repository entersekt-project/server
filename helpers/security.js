var CryptoJS = require('node-cryptojs-aes').CryptoJS;
const secretKey = "EasterEgg";

module.exports.encrypt = (value) => {
    try{
        return CryptoJS.AES.encrypt(value, secretKey.trim()).toString();
    }catch(error){
        console.log("error = ", error)
    }
   
}
module.exports.decrypted = (value) => { 
    try{
        return CryptoJS.AES.decrypt(value, secretKey.trim()).toString(CryptoJS.enc.Utf8);
    }catch(error){
        console.log("error = ", error)
    }
    
}