const jwt = require('jsonwebtoken');
const jwtSecretKey = "JWT_SECRET_KEY";

module.exports.genJWTToken = (loginUser) => {
      let data = {
          time: Date(),
          userId: loginUser._id,
      }  
      let token = jwt.sign(data, jwtSecretKey)
       return {
        _id: loginUser._id,
        name: loginUser.name,
        email: loginUser.email,
        token: token}
  }

  module.exports.veriJWTToken = (userId, token) => {    
      const verified = jwt.verify(token, jwtSecretKey);
      return verified.userId === userId
       
  }