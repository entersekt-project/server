
const security = require('../helpers/security')
const jwt = require('../helpers/jwt')
const LoginHistory = require('../models/loginHistory');
const Users = require('../models/users');
/* POST /api/login */
module.exports.createLoginEntry = async (user) => {
  try {
    return await LoginHistory.create({userId: user._id, email: user.email, date: new Date()});   
  } catch (error) {
    console.log(error)
    throw error;
  }
}

module.exports.getLoginHistory = async (userId, token) => {
  try {
    console.log("userId==================================")
    if(jwt.veriJWTToken(userId, token)){
      return await LoginHistory.find({userId:userId});   
    }
    throw {message: 'Please login first'} ; 
  } catch (error) {
    console.log(error)
    throw error;
  }
}

