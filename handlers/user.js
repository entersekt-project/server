
const security = require('../helpers/security')
const jwt = require('../helpers/jwt')
const Login = require('../models/loginHistory');
const Users = require('../models/users');

/* GET /api/login */
module.exports.authenticateUser = async (userDetails) => {
  try {
    let loginUser = await Users.findOne({email: userDetails.email});    
    if(!loginUser){
      throw {message: 'No user found'}
    }
      let decrypPass = security.decrypted(loginUser.password)
      if(decrypPass === userDetails.password){
        return jwt.genJWTToken(loginUser)
      }else{
        throw {message: 'Incorect Password or user'} ; 
      }
  } catch (error) {
    throw error
  }
};


/* POST /api/login */
module.exports.createUser = async (userDetails) => {
  try {
    let loginUser = await Users.findOne({email: userDetails.email});
    if(loginUser){
      throw {message: 'User exists please login'} ; 
    }
    userDetails.password = security.encrypt(userDetails.password)
    return await Users.create(userDetails);    
  } catch (error) {
    console.log(error)
    throw error;
  }
}
