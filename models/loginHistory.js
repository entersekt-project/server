const { ObjectId } = require('mongoose');
const mongoose = require('mongoose');

const { Schema, model } = mongoose;

const loginHistorySchema = new Schema({
  userId: ObjectId,
  email: String,
  date: Date,
  updated_at: { type: Date, default: Date.now },
});

module.exports = model('LoginHistory', loginHistorySchema);