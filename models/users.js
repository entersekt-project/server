const mongoose = require('mongoose');

const { Schema, model } = mongoose;

const userSchema = new Schema({
  name:String,
  email: String,
  password: String,
  updated_at: { type: Date, default: Date.now },
});

module.exports = model('User', userSchema);